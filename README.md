Bài 1.

Cho danh sách ghế sau: [

{SoGhe : 1 ,TenGhe : "số 1 " , Gia :100 , TrangThai :false}, {SoGhe : 2 ,TenGhe : "số 2 " , Gia :100 , TrangThai :false}, {SoGhe : 3 ,TenGhe : "số 3 " , Gia :100 , TrangThai :false}, {SoGhe : 4 ,TenGhe : "số 4 " , Gia :100 , TrangThai :false}, {SoGhe : 5 ,TenGhe : "số 5 " , Gia :100 , TrangThai :false}, {SoGhe : 6 ,TenGhe : "số 6 " , Gia :100 , TrangThai :false}, {SoGhe : 7 ,TenGhe : "số 7 " , Gia :100 , TrangThai :false}, {SoGhe : 8 ,TenGhe : "số 7 " , Gia :100 , TrangThai :false}, {SoGhe : 9 ,TenGhe : "số 9 " , Gia :100 , TrangThai :false}, {SoGhe :10 ,TenGhe : "số 10 " , Gia :100 , TrangThai :false}, {SoGhe :11 ,TenGhe : "số 11 " , Gia :100 , TrangThai :false}, {SoGhe :12 ,TenGhe : "số 12 " , Gia :100 , TrangThai :false}, {SoGhe :13 ,TenGhe : "số 13 " , Gia :100 , TrangThai :false}, {SoGhe :14 ,TenGhe : "số 14 " , Gia :100 , TrangThai :false}, {SoGhe :15 ,TenGhe : "số 15 " , Gia :100 , TrangThai :false}, {SoGhe :16 ,TenGhe : "số 16 " , Gia :100 , TrangThai :false}, {SoGhe :17 ,TenGhe : "số 17 " , Gia :100 , TrangThai :false}, {SoGhe :18 ,TenGhe : "số 18 " , Gia :100 , TrangThai :false}, {SoGhe :19 ,TenGhe : "số 19 " , Gia :100 , TrangThai :false}, {SoGhe :20 ,TenGhe : "số 20 " , Gia :100 , TrangThai :false}, {SoGhe :21 ,TenGhe : "số 21 " , Gia :100 , TrangThai :false}, {SoGhe :22 ,TenGhe : "số 22 " , Gia :100 , TrangThai :false}, {SoGhe :23 ,TenGhe : "số 23 " , Gia :100 , TrangThai :false}, {SoGhe :24 ,TenGhe : "số 24 " , Gia :100 , TrangThai :false}, {SoGhe :25 ,TenGhe : "số 25 " , Gia :100 , TrangThai :false}, {SoGhe :26 ,TenGhe : "số 26 " , Gia :100 , TrangThai :false}, {SoGhe :27 ,TenGhe : "số 27 " , Gia :100 , TrangThai :false}, {SoGhe :28 ,TenGhe : "số 28 " , Gia :100 , TrangThai :false}, {SoGhe :29 ,TenGhe : "số 29 " , Gia :100 , TrangThai :false}, {SoGhe :30 ,TenGhe : "số 30 " , Gia :100 , TrangThai :true}, {SoGhe :31 ,TenGhe : "số 31 " , Gia :100 , TrangThai :false}, {SoGhe :32 ,TenGhe : "số 32 " , Gia :100 , TrangThai :false}, {SoGhe :33 ,TenGhe : "số 33 " , Gia :100 , TrangThai :false}, {SoGhe :34 ,TenGhe : "số 34 " , Gia :100 , TrangThai :false}, {SoGhe :35 ,TenGhe : "số 35 " , Gia :100 , TrangThai :false},]

Yêu cầu:

1. render ra giao diện như hình : Bao gồm 4 Component: Home, DanhSachGhe, GheItem, DanhSachGheDangDat.

http://lms.myclass.vn/pluginfile.php/22523/mod_assign/intro/82472749_181180679950241_8186316182819700736_n.png

2.Khi người dùng nhấn chọn ghé, hiện danh sách ghế được chọn ra bên phải.Để làm được , ta cần một mảng danhSachGheDangDat: [], chọn ghế nào thì push vào mảng .Nếu đang chọn mà nhấn lại thì coi như huỷ, xoá ra khỏi mảng.

3.Khi nhấn chọn, chuyển ghế sang màu xanh lá cây. Gợi ý: đặt một biến state isBooking true thì đổi xanh, false thì trở lại bình thường

4.Mỗi đối tượng ghế có một thuộc tính TrangThai , bằng true nghĩa là ghế này đã bị đặt trước đó , không được đặt nữa, thì disable button ghế đi, chuyển sang màu đỏ

5.Chức năng chỗ nút Huỷ bỏ, không cần làm.

****Bài 2:
Yêu cầu bài tập :

1.Cho API lấy danh sách câu hỏi, tiến hành render danh sách câu hỏi ra màn hình , Giao diện giống như bài trắc nghiệm đã làm ở ES6

   https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions

   METHOD: GET

  - Có 2 loại câu hỏi là chọn đáp án đúng và loại điền vào chỗ trống, tạo 2 component tương ứng với 2 loại câu hỏi đó

  - Mỗi đối tượng câu hỏi đều có thuộc tính questionType, dựa vào đó để khi map từ danh sahcs câu hỏi sang danh sách component cho phù hợp

2. Làm bài test và chấm điểm:

Gợi ý: - tạo thêm một state là DanhSachDapAn,

           - User chọn hoặc điền đáp án thì sẽ push vào DanhSachDapAn một object như sau: {QuestionId:''", answer: {content: 'nội dung câu trả lời', exact : 'true hay false'}. Nếu question đó đã chọn đáp rồi, thì ko push vào nữa, mà chỉ sửa lại answer mới chọn.

           - Đối với loại câu hỏi chọn đáp án đúng, mỗi đáp án đều có thuộc tính exact, dựa vào để check

           - Đối với loại câu hỏi điền vào chỗ trống, đáp án chỉ có một, so sánh nếu value người dùng nhập === content thì là đúng, khác là sai, sau đó chuyển thành {QuestionId:''", answer: {content: 'nội dung câu trả lời', exact : 'true hay false'} và cũng làm như trên

         - Cuối cùng, khi người dùng nhấn nút submit để nộp bài, thì duyệt mảng DanhSachDapAn, kiểm tra từng item, câu nào có exact là true thì cộng 1 điểm, in ra màn hình tổng số điểm

