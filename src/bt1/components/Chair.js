import React, { useState } from 'react'

const Chair = ({ item, onClick }) =>{
    const [reserved, setReserved ] = useState(false)
    const { SoGhe, TrangThai } = item
    const changeReserved = (SoGhe) => {
        setReserved(!reserved)
        onClick(SoGhe)
    }
    const styleState = (
        TrangThai && !reserved ? { background: "#ff0000b3"}: reserved ? {background: "green"} : {}
    )
    return (
        <button
         className="chair"
         onClick={()=>changeReserved(SoGhe)}
         style={styleState}
         disabled={TrangThai}
            >{ SoGhe }</button>
    )
}
export default Chair;