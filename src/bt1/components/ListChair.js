
import React from 'react'
import Chair from './Chair';

const ListChair = ({ listChairData, onClick }) =>{
    return (
        <div className="listchair-container">
            { listChairData.map(item =>
                <Chair
                    key={'list' + item.SoGhe}
                    item={item}
                    onClick={onClick} />)
            }
        </div>

    )
}
export default ListChair;