import React from 'react'
import ListChair from './ListChair'
import listChairData from '../../listChairData'
import ChairReserved from './ChairReserved'
import './home.css'
import App from '../../App'

class BT1 extends React.Component {
    constructor(){
        super();
        this.state = {
            listReserved: [],
        }

    }
    addReserved = (SoGhe) => {
        const listReserved = this.state.listReserved.slice();
        const indexChair = listReserved.findIndex(el=>el === SoGhe);
        if(indexChair !== -1){
            listReserved.splice(indexChair, 1);
        }else {
            listReserved.push(SoGhe);
        }
        this.setState({listReserved});
    }
    cancelReserved = (SoGhe) => {
        const listReserved = this.state.listReserved.slice();
        const indexChair = listReserved.findIndex(el=>el === SoGhe);
        if(indexChair !== -1){
            listReserved.splice(indexChair, 1);
            this.setState({listReserved});
        }

    }
    render(){
        const isReserved = (SoGhe) => (chair) => SoGhe.indexOf(chair.SoGhe) !== -1;
        const getReadableReversed = (listData, SoGhe) =>
            listData.filter(isReserved(SoGhe));
        const listChairReversed = getReadableReversed(listChairData, this.state.listReserved)
        return (
            <App>
                <h1 className="header-title">
                            Dặt vé xe bus hãng cybersoft
                        </h1>
                <div className="main-container fix-container">
                    <div className="box-container bus">
                        <h2>Tài xế</h2>
                        <ListChair listChairData={listChairData} onClick={this.addReserved}  />
                    </div>
                    <div className="box-container chairs-reserved">
                        <h2>Danh sách ghế đã đặt ({ this.state.listReserved.length })</h2>
                        <ul>
                            { listChairReversed
                                            .map(item=>
                                                <ChairReserved
                                                    key={'reserved ' + item.TenGhe}
                                                    item={item}
                                                    cancelReserved={this.cancelReserved}
                                                    />
                                                )}

                        </ul>
                    </div>
                </div>
            </App>

        )
    }

}
export default BT1;