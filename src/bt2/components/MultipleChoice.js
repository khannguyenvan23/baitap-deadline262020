import React, { useRef, useState } from 'react'
import { useDispatch } from 'react-redux'
import Question, { ABCD, checkCorrect }  from './Question'
import { checkCorrectAnswer } from '../redux/action'

const Answer = ({answer, index, questionID, checkUserAnswer, checkRef}) =>  {

 return <div className="question">
    <input value={answer.id}
    className={`answer-${answer.id}`}
    type="radio"
    name={`answer-${questionID}`}
    ref={checkRef}
    onClick={checkUserAnswer}
    />
    <label>{` ${ABCD[index]}  ${answer.content} ` }</label>
</div>
}

const MultipleChoice = ({question, answers, index, id}) => {
    const checkRef = useRef(null)
    const dispatch = useDispatch()
    const checkUserAnswer = (inputEl) =>{
        const userAnswer  = inputEl.currentTarget.value
        const correctAnswer = answers.find((item)=> item.exact === true).content
        answers.forEach(answer => {

            if(answer.id === userAnswer){
                dispatch(checkCorrectAnswer({
                    questionID: id,
                    check: answer.exact,
                    correctAnswer: correctAnswer
                }))
            }
        });

    }
    return ( <Question
        index ={index}
         question={question}
         questionID={id}
         key={'keyQuestion'+index}>

            { answers.map((answer, i)=>(
                <Answer
                    key={'keyAnswer' + i}
                    answer={answer}
                    index={i}
                    questionID={'ques' + index}
                    checkUserAnswer={checkUserAnswer}
                    checkRef={checkRef}
                    />
            )) }
    </Question> )
}
export default MultipleChoice;
