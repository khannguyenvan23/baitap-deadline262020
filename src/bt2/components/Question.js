import React from 'react'
import { useSelector } from 'react-redux';
export const ABCD = ['A', 'B', 'C', 'D']
export const checkCorrect = false;
const Question = ({index, children, question, questionID}) =>{
    const checkSubmit = useSelector((state)=> state.submitAnswer)
    const userAnswer = useSelector((state)=>state.userAnswer);

    const findWrong =  userAnswer.find((el)=>el.questionID === questionID)
    return (
        <div className="answers-container">
            <div className="question-title">
                <p>Câu {index + 1} : {question}</p>
                {children}
            </div>
            {
                findWrong && !findWrong.check && checkSubmit ? <p style={{
                display: "block",
                color:"red",margin: "10px 0px 5px"
            }}>Bạn trả lời sai, đáp án đúng là {findWrong.correctAnswer}</p> :
            ''
            }

         </div>

    )
}

export default Question;