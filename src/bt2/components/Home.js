import React from 'react'
import App from '../../App.js';
import './home.css'
import FillnBlank from './FillInBlank.js';
import MultipleChoice from './MultipleChoice'
import { connect } from 'react-redux';
import Result from './Result.js';
import { fetchQuizz } from '../redux/action/index.js';
/**
 * TO DO
 * - lay id cau dung va cau sai vo obj
 * - Sau khi nguoi dung submit thi render so diem so cau dung va cau sai
 * - Render ra cho nao sai
 * - Reset choi lai tu dau
 */

/**
 * QUESTION TYPE
 */
const FILL_BLANK = 2;
const MUILTIPLE_CHOICE = 1;

class BT2 extends React.Component {

    componentDidMount(){


        this.props.dispatch(fetchQuizz())

        // Fetch
        // fetch('https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions')
        //     .then(res=>res.json())
        //     .then(questions=>{
        //             this.setState({questions})
        //             localStorage.setItem('d', JSON.stringify(questions))


        //     })
        //     .catch(error=>this.setState({error}));
    }

    render() {
        const getAppropriateQuestion = (type) => {
            return this.props.fetchQuizz.quizz.filter(question => question.questionType === type)
        }
        let multipleChoiceData;
        let FillBlankData;
        if(!this.props.fetchQuizz.isFetching){
            multipleChoiceData = getAppropriateQuestion(MUILTIPLE_CHOICE);
            FillBlankData =  getAppropriateQuestion(FILL_BLANK);
            // console.log(FillBlankData)
        }
        console.log()
        return <App>
          <h1 className="header-title">
                            Cybersoft Quizz
                        </h1>
            <div className="question-container fix-container">

            // Phan 1
                {
                    multipleChoiceData ? multipleChoiceData.map((item, index) => (
                        <MultipleChoice
                            key={'multiple' + index}
                            question={item.content}
                            answers={item.answers}
                            index={index}
                            id={item.id}

                            />
                    )): 'loading'
                }
            // Phan 2
                {
                    FillBlankData ? FillBlankData.map((item, index) => (
                        <FillnBlank
                            key={'fill' + index}
                            question={item.content}
                            index={index}
                            answers={item.answers[0]}
                            id={item.id}

                            />
                    )): 'loading'
                }

                <Result />
            </div>


        </App>
    }
}
const mapStateToProps = (state) => {

    return {
        fetchQuizz: state.FetchQuizzReducer,
        userAnswer: state.userAnswer,
    }
}

export default connect(mapStateToProps)(BT2)