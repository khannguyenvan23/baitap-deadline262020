import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
export default function Result() {
    const dispatch = useDispatch()
    const checkSubmit = useSelector((state)=> state.submitAnswer)
    const quizz = useSelector((state)=>state.FetchQuizzReducer.quizz)
    const userAnswer = useSelector((state)=>state.userAnswer);

    const soCauDung = userAnswer.filter((item)=> item.check === true).length;
    const soCau = quizz.length

    const soDiem =((10 /soCau) * soCauDung).toFixed(2);
    const letSubmit = soCau === userAnswer.length
    const result =  userAnswer.length === soCau && checkSubmit ? (
        <div className="ketqua">
            Bạn trả lời đúng <span style={{color: "red"}}>{soCauDung + '/' +  soCau}</span> câu:
            Số điểm của bạn là <span style={{color: "red"}}>{ soDiem }</span> điểm
        </div>) : ''

    const handleSubmit = () => {
        dispatch({
            type: 'SUBMIT_ANSWER',
        });
    }
    return (
        <div>
         <p style={letSubmit ? {
                display: "none",
                color:"red",margin: "10px 0px 5px"
            }: {
                display: "block",
                color:"red",margin: "10px 0px 5px"
            }}>Bạn cần phải trả lời hết câu hỏi mới được kiểm tra kết quả</p>
            <button onClick={handleSubmit} className="submit-result" style={letSubmit ? {
                display: "block"
            }: {
                display: "none"
            }} >Submit kết quả</button>
            <div>

             { result }
            </div>
        </div>
    )

}