import React, {  useState } from 'react'
import Question, { checkCorrect } from './Question'
import { checkCorrectAnswer } from '../redux/action'
import { useDispatch } from 'react-redux'

const FillnBlank = ({question, answers, index, id}) => {
    const [answerText , setAnswerText ] = useState('')
    const dispatch = useDispatch()
    const checkAnswer = (inputEl) => {
        const userAnswer = inputEl.currentTarget.value.toLowerCase()
        setAnswerText(userAnswer)
        const correctAnswer = answers.content.toLowerCase()

        if(answerText !== '' ){
            // make new obj gom id, cau dung cau sai,
            dispatch(checkCorrectAnswer({
                questionID: id,
                check: correctAnswer === userAnswer,
                correctAnswer: answers.content
            }))
        }

    }

    return <Question
        index ={index}
         question={question}
         key={'keyQuestion'+index}
         questionID={id}>

        <input
            value={answerText}
            type="text"
            onChange={checkAnswer}
        />
    </Question>
}
export default FillnBlank;
