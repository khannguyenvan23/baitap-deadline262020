import { createStore, applyMiddleware, combineReducers, compose  } from 'redux'
import { fetchQuizz } from '../redux/action'
import { userAnswer,submitAnswer } from '../redux/reducer/quizzReducer'
import FetchQuizzReducer from '../redux/reducer/fetchQuizz'
import thunk from "redux-thunk";
const reducer = combineReducers({
  submitAnswer,
    userAnswer,
    FetchQuizzReducer,
  });

const store = createStore(
    reducer,
   applyMiddleware(thunk)

)

// console.log(store.getState());
// const unsubcribe = store.subscribe(() => console.log(store.getState()))
// store.dispatch(fetchQuizz()).then(async () =>await console.log(store.getState()))
// unsubcribe();
export { store }