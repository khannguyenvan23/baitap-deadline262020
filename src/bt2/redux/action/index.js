
/**
 * ACTION TYPE
*/

export const CHECK_USER_ANSWER = 'CHECK_USER_ANSWER'

/**
 * ACTION CREATOR
 */

export const checkCorrectAnswer = (payload) => {
    return {
        type: CHECK_USER_ANSWER,
        payload
    }
}

export const REQUEST_QUIZZ = 'REQUEST_QUIZZ'
function requestQuizz(quizz) {
  return {
    type: REQUEST_QUIZZ,
    quizz
  }
}

export const RECEIVE_QUIZZ = 'RECEIVE_QUIZZ'
function receiveQuizz(json) {
  return {
    type: RECEIVE_QUIZZ,
    quizz: json,
  }
}
export const ERROR_QUIZZ = 'ERROR_QUIZZ'
function errorQuizz(error){
    return {
        type: ERROR_QUIZZ,
        error
    }
}

export function fetchQuizz(quizz){
    return (dispatch) => {
        dispatch(requestQuizz(quizz))
        return fetch(`https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions`)
      .then(
        response => response.json()
        // Do not use catch, because errors occured during rendering
        // should be handled by React Error Boundaries
        // https://reactjs.org/docs/error-boundaries.html
      )
      .then(json =>
        // We can dispatch many times!
        // Here, we update the app state with the results of the API call.
        {
            localStorage.setItem('d', JSON.stringify(json))
            dispatch(receiveQuizz(json))
        }
      )
      .catch(error=>dispatch(errorQuizz(error)))
    }
}