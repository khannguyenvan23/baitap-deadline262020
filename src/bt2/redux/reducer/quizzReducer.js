
import { CHECK_USER_ANSWER } from "../action";
export function submitAnswer(state = false, {type, payload}){
    switch(type){
        case 'SUBMIT_ANSWER':
            return true
        default:
            return state
    }
}
export function userAnswer(state = [], {type, payload}) {
    switch(type){
        case CHECK_USER_ANSWER:
            const copyState = [...state]
            const index = copyState.findIndex((item) =>(item.questionID === payload.questionID))
            if(index !== -1){
                copyState[index] = payload
                return copyState
            }
            copyState.push(payload)
            return copyState
        default: {
            return state
        }
    }
}

