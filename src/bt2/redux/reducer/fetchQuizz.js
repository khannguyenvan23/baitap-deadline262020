import { REQUEST_QUIZZ, RECEIVE_QUIZZ, ERROR_QUIZZ } from '../action'
export default function FetchQuizzReducer(state = {
    isError: false,
    errorMessage: '',
    isFetching: false,
    quizz: [],
}, action){
    switch(action.type){
        case ERROR_QUIZZ:
            console.log(action.error)
            return Object.assign({}, state, {
                isError: true,
                errorMessage: action.error.toString()
            })
        case REQUEST_QUIZZ:
            const quizz =  JSON.parse(localStorage.getItem('d'))
            if(!action.quizz && quizz){
                return Object.assign({}, state, {
                    isError: false,
                    isFetching: false,
                    quizz: quizz,
                })
            }
            return Object.assign({}, state, {
                isError: false,
                isFetching: true,
            })

        case RECEIVE_QUIZZ:
             // localStorage

            return Object.assign({}, state, {
                isError: false,
                errorMessage:'',
                isFetching: false,
                quizz: action.quizz,
            })
        default:
            return state;
    }
}