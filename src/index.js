import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import BT1 from './bt1/components/Home';
import BT2 from './bt2/components/Home';
import App, { Home } from './App';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { store } from './bt2/config/store';
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
  <Switch>
    <Route exact path='/' component={Home} />
    <Route path='/bt1' component={BT1} />
    <Route path='/bt2' component={BT2} />
  </Switch>
  </BrowserRouter>
</Provider>
,
  document.getElementById('root')
);
